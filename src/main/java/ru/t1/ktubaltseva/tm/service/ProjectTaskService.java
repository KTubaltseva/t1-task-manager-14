package ru.t1.ktubaltseva.tm.service;

import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.IProjectTaskService;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        final List<Task> projectTasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : projectTasks) taskRepository.removeById(task.getId());
        return projectRepository.removeById(projectId);
    }

    @Override
    public Task unbindTaskFromProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        if (!projectRepository.existsById(projectId)) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
